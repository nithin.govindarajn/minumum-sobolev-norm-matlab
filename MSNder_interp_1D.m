function [y, yder]=MSNder_interp_1D(x, f, f_der, xprime, Opt, M)
% Code template borrowed from Karthik's work. This function includes
% derivative data in the interpolation.


%Normalize the input/output nodes to (-1,1)
a = min(x); b = max(x);
x = 2*(x-a)/(b-a) - 1;
const = 2/(b-a);
xprime = 2*(xprime-a)/(b-a) - 1;

if(size(x,1) == 1), x = x'; end
if(size(xprime,1) == 1), xprime = xprime';end
if(size(f,1) == 1), f = f'; end
if(size(f_der,1) == 1), f_der = f_der'; end

% Double the size of M to 2*M since, now we have 2*N (derivative and value)
M = 2*M;

%Construct the Chebyshev Vandermonde
VNM = cheby1b(x, M-1); %NxM matrix
VNM_der = const*cheby1der_b(x,M-1);
Dinv = diag([1:M].^(-Opt));

% solve for coeff
A = [VNM;VNM_der]*Dinv;
[U,~,~] = svd(A);

Atilde = U'*A;
ftilde = U'*[f;f_der];

[Q,R]=qr((Atilde)',0);
coeff = Dinv * ( Q*(R'\ftilde));

%evaluate function and derivative
Vo = cheby1b(xprime, M-1);
Vo_der = const*cheby1der_b(xprime, M-1);
y = Vo*coeff;
yder = Vo_der*coeff;


