function V = cheby2derx_b(x, y, Mx, My)
% Chebyshev polynomials of the first kind basis
if(size(x,1) == 1), x = x'; end
if(size(y,1) == 1), y = y'; end

% x-component
U = zeros(length(x),Mx);

% first column
U(:,1) = ones(length(x),1);
% second column
U(:,2) = 2.*x;
for k = 3:Mx;
    U(:,k) = 2.*x.*U(:,k-1) - U(:,k-2);  
end
for k=1:Mx;
    U(:,k) = k*U(:,k);
end

Vxder = [zeros(length(x),1),  U];






% y-component
Vy = zeros(length(y),My+1);

% first column
Vy(:,1) = ones(length(y),1);
% second column
Vy(:,2) = y;

for k = 3:My+1
   Vy(:,k) = 2.*y.*Vy(:,k-1) - Vy(:,k-2);
end


% combine them
V = kr(Vxder',Vy')';