function V = cheby2dery_b(x, y, Mx, My)
% Chebyshev polynomials of the first kind basis
if(size(x,1) == 1), x = x'; end
if(size(y,1) == 1), y = y'; end



% x-component
Vx = zeros(length(x),Mx+1);

% first column
Vx(:,1) = ones(length(x),1);
% second column
Vx(:,2) = x;

for k = 3:My+1
   Vx(:,k) = 2.*x.*Vx(:,k-1) - Vx(:,k-2);
end




% y-component
U = zeros(length(y),My);

% first column
U(:,1) = ones(length(y),1);
% second column
U(:,2) = 2.*y;
for k = 3:My;
    U(:,k) = 2.*y.*U(:,k-1) - U(:,k-2);  
end
for k=1:My;
    U(:,k) = k*U(:,k);
end

Vyder = [zeros(length(y),1),  U];








% combine them
V = kr(Vx',Vyder')';