function [y, yder] = MSN_interp_1D(x, f, xprime, Opt, M)
% Used Kartik's code as template.


%Normalize the input/output nodes to (-1,1)
a = min(x); b = max(x);
x = 2*(x-a)/(b-a) - 1;
const = 2/(b-a);
xprime = 2*(xprime-a)/(b-a) - 1;

if(size(x,1) == 1), x = x'; end
if(size(xprime,1) == 1), xprime = xprime';end
if(size(f,1) == 1), f = f'; end

%Construct the Chebyshev Vandermonde
VNM = cheby1b(x, M-1); %cos(acos(x)*[0:M-1]); %NxM matrix 
Dinv = diag([1:M].^(-Opt));


% solve for coeff
A = VNM*Dinv;
[U,~,~] = svd(A);

Atilde = U'*A;
ftilde = U'*f;

[Q,R]=qr((Atilde)',0);
coeff = Dinv * ( Q*(R'\ftilde));


%evaluate function and derivative
Vo = cheby1b(xprime, M-1);
Vo_der = const*cheby1der_b(xprime, M-1);
y = Vo*coeff;
yder = Vo_der*coeff;




