function V = cheby1b(xi, c)
% Chebyshev polynomials of the first kind basis
if(size(xi,1) == 1), xi = xi'; end

V = zeros(length(xi),c+1);

% first column
V(:,1) = ones(length(xi),1);
% second column
V(:,2) = xi;

for k = 3:c+1
   V(:,k) = 2.*xi.*V(:,k-1) - V(:,k-2);
end
