function [z, zx, zy] = MSN_interp2D(xy, f, xyprime, Opt,Mxy)
% Used kartik's code as template.


%The data
x = xy(:,1);
y = xy(:,2);
xprime = xyprime(:,1);
yprime = xyprime(:,2);


if(size(x,1) == 1), x = x'; end
if(size(y,1) == 1), y = y'; end
if(size(f,1) == 1), f = f'; end


% Normalize to -1 to 1 interval
ax = min(x); bx = max(x); ay = min(y); by = max(y); 
x = 2*(x-ax)./(bx-ax) - 1;
xprime = 2*(xprime-ax)./(bx-ax) - 1;
const_x = 2/(bx-ax);
y = 2*(y-ay)./(by-ay) - 1;
const_y = 2/(by-ay);
yprime = 2*(yprime-ay)./(by-ay) - 1;

%Construct the Chebyshev Vandermonde
Mx =Mxy(1); My = Mxy(2);
M = Mx*My;
V = cheby2b(x, y, Mx-1, My-1);
D_inv = zeros(M,1);
for l=0:Mx-1
    D_inv((l*My + 1):(l+1)*My,1) = (1+l^2+(0:My-1).^2).^(-Opt/2);
end
[D_inv,I] = sort(D_inv,'descend');
V = V(:,I);
Dinv = diag(D_inv);

% solve for coeff
A = V*Dinv;
[U,~,~] = svd(A);

Atilde = U'*A;
ftilde = U'*f;

[Q,R]= qr((Atilde)',0);
coeff = Dinv * (Q*(R'\ftilde));


%evaluate function and derivative
Vo = cheby2b(xprime, yprime, Mx-1, My-1);
Vo = Vo(:,I);
Vo_x = const_x*cheby2derx_b(xprime, yprime, Mx-1, My-1);
Vo_x = Vo_x(:,I);
Vo_y = const_y*cheby2dery_b(xprime, yprime, Mx-1, My-1);
Vo_y = Vo_y(:,I);

z = Vo*coeff;
zx = Vo_x *coeff;
zy = Vo_y *coeff;


