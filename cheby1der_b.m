function Vder = cheby1der_b(xi, c)
% Chebyshev polynomials of the first kind basis
if(size(xi,1) == 1), xi = xi'; end


U = zeros(length(xi),c);

% first column
U(:,1) = ones(length(xi),1);

% second column
U(:,2) = 2.*xi;

for k = 3:c;
    U(:,k) = 2.*xi.*U(:,k-1) - U(:,k-2);  
end
for k=1:c;
    U(:,k) = k*U(:,k);
end

Vder = [zeros(length(xi),1),  U];
