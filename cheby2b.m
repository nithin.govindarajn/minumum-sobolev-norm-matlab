function V = cheby2b(x, y, Mx, My)
% Chebyshev polynomials of the first kind basis
if(size(x,1) == 1), x = x'; end
if(size(y,1) == 1), y = y'; end

% x-component
Vx = zeros(length(x),Mx+1);

% first column
Vx(:,1) = ones(length(x),1);
% second column
Vx(:,2) = x;

for k = 3:Mx+1
   Vx(:,k) = 2.*x.*Vx(:,k-1) - Vx(:,k-2);
end

% y-component
Vy = zeros(length(y),My+1);

% first column
Vy(:,1) = ones(length(y),1);
% second column
Vy(:,2) = y;

for k = 3:My+1
   Vy(:,k) = 2.*y.*Vy(:,k-1) - Vy(:,k-2);
end

% combine them
V = kr(Vx',Vy')';